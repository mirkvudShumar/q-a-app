import React from 'react';
import classes from './App.css';
import QaTool from './containers/QaTool/QaTool'

function App() {
  return (
  	<div className={classes.Content}>
  		<QaTool />
  	</div>
  );
}

export default App;
